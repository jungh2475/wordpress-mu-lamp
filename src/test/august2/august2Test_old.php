<?php

require_once(rtrim($_SERVER['DOCUMENT_ROOT'], '/') . '/wp-load.php');
require_once('august2.php');


class August2Tests extends PHPUnit_Framework_TestCase
{
	private $august2;

	protected function setUp()
	{
		$this->august2 = new August2();
	}

	protected function tearDown()
	{
		$this->august2 = NULL;
	}

	public function testXAdd()
	{
		$result = $this->august2->Xadd(1, 2);
		$this->assertEquals(3, $result);
	}
	
	public function addDataProvider() {
		return array(
				array(1,2,3),
				array(0,0,0),
				array(-1,-1,-2),
		);
	}
	
	/* @dataProvider addDataProvider */
	public function testAdd($a, $b, $expected)
	{
		$result = $this->calculator->add($a, $b);
		$this->assertEquals($expected, $result);
	}
	
	//mock
	public function testWithStub()
	{
		// Create a stub for the Calculator class.
		$august2 = $this->getMockBuilder('Calculator')
		->getMock();
	
		// Configure the stub.
		$august2->expects($this->any())
		->method('add')
		->will($this->returnValue(6));
	
		$this->assertEquals(6, $august2->add(100,100));
	}

}


/* 
 * https://phpunit.de/getting-started.html
 * https://www.startutorial.com/articles/view/phpunit-beginner-part-1-get-started
 * how to install(osx) using brew?
 * wget https://phar.phpunit.de/phpunit.phar
 * chmod +x phpunit.phar, sudo mv phpunit.phar /usr/local/bin/phpunit  or
 * php phpunit.phar --version
 * phpunit --version, phpunit CalculatorTest.php
 * phpunit --bootstrap src/Email.php tests/EmailTest
 * phpunit --bootstrap src/Email.php tests  // tests/*Test.php
 */

?>