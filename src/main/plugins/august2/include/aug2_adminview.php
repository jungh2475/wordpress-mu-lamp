<?php
//https://www.smashingmagazine.com/2016/04/three-approaches-to-adding-configurable-fields-to-your-plugin/

//require wordpress-load.php

//Save Plugin or Theme Setting Options using AJAX in WordPress
//https://www.wpoptimus.com/434/save-plugin-theme-setting-options-ajax-wordpress/
//add_action( 'admin_init', 'myPlugin_admin_scripts' ); -> <script>..document.ready->>$(#id).click(functionName);
//http://wordpress.stackexchange.com/questions/79898/trigger-custom-action-when-setting-button-pressed

/*
 *  개발 roadmap ******************************
 *  0. common
 *    a)shortcode vbox, 
 *    b)export-import
 *    c)service status
 *    d1)vuejs+rest, d2)angular
 *    
 *  1. woocommerce extra
 *    a) redirect, b)augment comments, c) promotions
 *    d) order export, e)auto product registration
 *     
 *  2. Extra Pack2
 *  
 *    a) seo
 * 
 *  3. erp : sales, hr, marketting,
 *  
 *  4. 
 * 
 */

class Aug2_Setting {
	public function __construct(){
		
	}
	
	public function init(){
		//add_action
		add_action( 'admin_menu', array( $this, 'create_plugin_settings_page' ) );
		add_action( 'admin_init', array( $this, 'setup_sections_fields' ) );
		
	}
	
	public function setup_sections_fields() {
		add_settings_section( 'our_first_section', 'My First Section Title', false, 'smashing_fields' );
		add_settings_section( 'our_second_section', 'My Second Section Title', array( $this, 'section_callback' ), 'smashing_fields' );
		add_settings_section( 'our_third_section', 'My Third Section Title', array( $this, 'section_callback' ), 'smashing_fields' );
	
		add_settings_field( 'our_first_field', 'Field Name', array( $this, 'field_callback' ), 'smashing_fields', 'our_first_section' );
		//or
		$fields = array(
				array(
						'uid' => 'our_first_field',
						'label' => 'Awesome Date',
						'section' => 'our_first_section',
						'type' => 'text',
						'options' => false,
						'placeholder' => 'DD/MM/YYYY',
						'helper' => 'Does this help?',
						'supplemental' => 'I am underneath!',
						'default' => '01/01/2015'
				)
		);
		foreach( $fields as $field ){
			add_settings_field( $field['uid'], $field['label'], array( $this, 'field_callback' ), 'smashing_fields', $field['section'], $field );
			register_setting( 'smashing_fields', $field['uid'] );
		}
		
	}
	
	public function section_callback( $arguments ) {
		echo "hihi i was called";
		switch( $arguments['id'] ){
			case 'our_first_section':
				echo 'This is the first description here!';
				break;
			case 'our_second_section':
				echo 'This one is number two';
				break;
			case 'our_third_section':
				echo 'Third time is the charm!';
				break;
		}
	}
	
	public function field_callback( $arguments ) {
		echo '<input name="our_first_field" id="our_first_field" type="text" value="' . get_option( 'our_first_field' ) . '" />';
		register_setting( 'smashing_fields', 'our_first_field' );
		
		$value = get_option( $arguments['uid'] ); // Get the current value, if there is one
		if( ! $value ) { // If no value exists
			$value = $arguments['default']; // Set to our default
		}
		
		// Check which type of field we want
		switch( $arguments['type'] ){
			case 'text': // If it is a text field
				printf( '<input name="%1$s" id="%1$s" type="%2$s" placeholder="%3$s" value="%4$s" />', $arguments['uid'], $arguments['type'], $arguments['placeholder'], $value );
				break;
		}
		
	}
	
	public function create_plugin_settings_page() {
		// Add the menu item and page
		$page_title = 'My Awesome Settings Page';
		$menu_title = 'Awesome Plugin';
		$capability = 'manage_options';
		$slug = 'smashing_fields';
		$callback = array( $this, 'plugin_settings_page_content' );
		$icon = 'dashicons-admin-plugins';
		$position = 100;
	
		add_menu_page( $page_title, $menu_title, $capability, $slug, $callback, $icon, $position );
		//add_submenu_page( 'options-general.php', $page_title, $menu_title, $capability, $slug, $callback );
	}
	
	public function plugin_settings_page_content() {
		echo 'Hello World!';
		/*  ?>
		 *  <div class="wrap">
		<h2>My Awesome Settings Page</h2>
		<form method="post" action="options.php">
            <?php
                settings_fields( 'smashing_fields' );
                do_settings_sections( 'smashing_fields' );
                submit_button();
            ?>
		</form>
	</div> <?php
		 * 
		 */
		
		
	}
	
}


/*
 * woocommerce_wp_text_input(), woocommerce_wp_textarea_input(), woocommerce_wp_select(), woocommerce_wp_checkbox(), woocommerce_wp_hidden_input()
 * http://www.remicorson.com/mastering-woocommerce-products-custom-fields/
 * 자료를 get_post_meta를 가지고 와서 $param을 만들고, 모양을 만들고,  save_button을 누르면 form 혹은 ajax로 저장('woocommerce_process_product_meta', update_post_meta)
 */

//좀더 진화한 class 사용한 버젼 : https://code.tutsplus.com/tutorials/adding-custom-fields-to-simple-products-with-woocommerce--cms-27904
class Ag2_Product_Extra_Field {
	
	private $textfield_id;
	public function __construct() {
		$this->textfield_id = 'tutsplus_text_field';
	}
	public function init() {
		add_action('woocommerce_product_options_grouping',array( $this, 'product_options_grouping' ));
		add_action('woocommerce_process_product_meta',array( $this, 'add_custom_linked_field_save' ));
	}
	public function product_options_grouping() {
		$description = sanitize_text_field( 'Enter a description that will be displayed for those who are viewing the product.' );
		$placeholder = sanitize_text_field( 'Tease your product with a short description.' );
		
		$args = array(
				'id'            => $this->textfield_id,
				'label'         => sanitize_text_field( 'Product Teaser' ),
				'placeholder'   => 'Tease your product with a short description',
				'desc_tip'      => true,
				'description'   => $description,
		);
		woocommerce_wp_text_input( $args );
	}
	public function add_custom_linked_field_save( $post_id ) {
	
		if ( ! ( isset( $_POST['woocommerce_meta_nonce'], $_POST[ $this->textfield_id ] ) || wp_verify_nonce( sanitize_key( $_POST['woocommerce_meta_nonce'] ), 'woocommerce_save_data' ) ) ) {
			return false;
		}
	
		$product_teaser = sanitize_text_field(
				wp_unslash( $_POST[ $this->textfield_id ] )
				);
	
		update_post_meta(
				$post_id,
				$this->textfield_id,
				esc_attr( $product_teaser )
				);
	}
}


function woo_add_custom_general_fields() {

	global $woocommerce, $post;

	echo '<div class="options_group">';

	// Custom fields will be created here...
	// Text Field
	woocommerce_wp_text_input(
			array(
					'id'          => '_text_field',
					'label'       => __( 'My Text Field', 'woocommerce' ),
					'placeholder' => 'http://',
					'desc_tip'    => 'true',
					'description' => __( 'Enter the custom value here.', 'woocommerce' )
			)
			);
	
	$custom_field_type = get_post_meta( $post->ID, '_custom_field_type', true );
	//echo get_post_meta( $post->ID, 'my-field-slug', true );
	
	$param = array( 
		'id'                => '_number_field', 
		'label'             => __( 'My Number Field', 'woocommerce' ), 
		'placeholder'       => '', 
		'description'       => __( 'Enter the custom value here.', 'woocommerce' ),
		'type'              => 'number', 
		'custom_attributes' => array(
				'step' 	=> 'any',
				'min'	=> '0'
			) 
	);
	
	woocommerce_wp_text_input($param);
	echo '</div>';

}

function woo_add_custom_general_fields_save( $post_id ){
	// Text Field
	$woocommerce_text_field = $_POST['_text_field'];
	if( !empty( $woocommerce_text_field ) )
		update_post_meta( $post_id, '_text_field', esc_attr( $woocommerce_text_field ) );
	
	// Number Field
	$woocommerce_number_field = $_POST['_number_field'];
	if( !empty( $woocommerce_number_field ) )
		update_post_meta( $post_id, '_number_field', esc_attr( $woocommerce_number_field ) );
	
}

/*
add_action( 'woocommerce_product_write_panel_tabs', 'woo_add_custom_admin_product_tab' );

function woo_add_custom_admin_product_tab() {
	?>
	<li class="custom_tab"><a href="#custom_tab_data"><?php _e('My Custom Tab', 'woocommerce'); ?></a></li>
	<?php
}
*/

// Display Fields
add_action( 'woocommerce_product_options_general_product_data', 'woo_add_custom_general_fields' );
// Save Fields
add_action( 'woocommerce_process_product_meta', 'woo_add_custom_general_fields_save' );

?>