<?php

/*
 * 
 * https://bitbucket.org/kewtea_sangsoo/borderlnad_essential/src/020e5572f88b3b7a1c43acf912874c15f9b015a7/functions.php?at=master&fileviewer=file-view-default
 * 여기도 중요 객체 덩어리 wooc_product->list,product_tab, comment, header, footer 로 객체를 가지게 한다  
 */


class Aug2_view {}

//init()
add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 8;' ), 20 );
add_filter( 'woocommerce_product_tabs', 'wp_woo_rename_reviews_tab', 98);

//is_single(), global $post; isset($current_views), empty($cu.., is_numeric($cu

function wp_woo_rename_reviews_tab($tabs) {
	global $product;
	$id = $product->id;

	$args = array ('post_type' => 'product', 'post_id' => $id);
	$comments = get_comments( $args );

	$check_product_review_count = count($comments);

	$tabs['reviews']['title'] = 'Reviews('.$check_product_review_count.')';

	return $tabs;
}


/* redirect product detail page, (add_to_cart) */
function woocommerce_template_single_add_to_cart() {
	global $product;

	$product_id = $product->id;
	$product_type = get_post_meta( $product_id, 'product-type', true );
	if ('redirect' == $product_type) {
		$redirect_url = get_post_meta( $product_id, 'redirect-url', true );
		echo '<a href=//'.$redirect_url.' target="_blank" class="cart" style="display: table;">'.
				'<button type="submit" class="single_add_to_cart_button qbutton button alt" style="margin-left:0 !important;" >Redirect</button>'.
				'</a>';
	} else {
		do_action( 'woocommerce_' . $product->product_type . '_add_to_cart'  );
	}
}

//http://jeroensormani.com/redirect-users-after-add-to-cart/
//https://www.pootlepress.com/2014/10/replace-add-to-cart-button-woocommerce-shop-page/#
//https://www.templatemonster.com/help/woocommerce-how-to-change-add-to-cart-button-text.html#gref
//http://www.fix-css.com/2013/10/replace-add-to-cart-button-in-products-category-list-with-a-link-to-single-product-page-in-woocommerce/
//https://nicola.blog/2015/09/18/creating-custom-add-to-cart-url/
//https://businessbloomer.com/woocommerce-custom-add-cart-urls-ultimate-guide/
//https://www.skyverge.com/blog/how-to-override-woocommerce-template-files/
//https://wisdmlabs.com/blog/override-woocommerce-templates-plugin/
//https://www.themelocation.com/how-to-hideremovedisable-add-to-cart-button-in-woocommerce/
//https://www.warrenchandler.com/2015/01/25/move-the-cart-in-a-woocommerce-single-product/
function wooc_redirect_button(){}

/*
 * 버튼 위의 글자 바꾸기 ******************
 * 중요한 hook은 woocommerce_single_product_summary(action), woocommerce_after_shop_loop_item(action)
 * add_filter( 'woocommerce_product_single_add_to_cart_text', 'woo_custom_single_add_to_cart_text' ); 
 * function woo_custom_single_add_to_cart_text() { .....상황에 따라 다른 글자들이 나오게 ....return __( 'My Button Text', 'woocommerce' );
 */ 
  /*STEP 1 - REMOVE ADD TO CART BUTTON ON PRODUCT ARCHIVE (SHOP) */

function remove_loop_button(){
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
}
add_action('init','remove_loop_button');

/*STEP 2 -ADD NEW BUTTON THAT LINKS TO PRODUCT PAGE FOR EACH PRODUCT */

add_action('woocommerce_after_shop_loop_item','replace_add_to_cart');
function replace_add_to_cart() {
	//여기를 선택적으로 바꾸게 하자 
	global $product;
	$link = $product->get_permalink();
	echo do_shortcode('<br>[button link="' . esc_attr($link) . '"]Read more[/button]');
}


 /* 
 * 
 * 
 * remove button
 * remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart');
 * remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
 * add_filter('woocommerce_is_purchasable', 'my_woocommerce_is_purchasable', 10, 2);
 * function my_woocommerce_is_purchasable($is_purchasable, $product) {
 *       return ($product->id == whatever_mambo_jambo_id_you_want ? false : $is_purchasable);
 * }
 * 
 * 
 * 
 * Override WooCommerce Templates
 * add_filter( 'woocommerce_locate_template', 'woo_adon_plugin_template', 1, 3 );
   function woo_adon_plugin_template( $template, $template_name, $template_path ) {
 * 
 */

//https://code.tutsplus.com/articles/customizing-comments-in-wordpress-functionality-and-appearance--wp-26128
function wooc_product_comments_extra(){
	
}

?>