f

woocommerce shortcode

https://github.com/woocommerce/woocommerce/blob/master/includes/class-wc-shortcodes.php
public static function init() {
		$shortcodes = array(
			'product'                    => __CLASS__ . '::product',
			'products'                   => __CLASS__ . '::products',
			'recent_products'            => __CLASS__ . '::recent_products',
			'related_products'           => __CLASS__ . '::related_products',
			'woocommerce_order_tracking' => __CLASS__ . '::order_tracking',
			.....
		);
		
		foreach ( $shortcodes as $shortcode => $function ) {
			add_shortcode( apply_filters( "{$shortcode}_shortcode_tag", $shortcode ), $function );
		}

		
-----------------
public static function recent_products( $atts ) {
		$atts = shortcode_atts( array(
			'per_page' => '12',
			'columns'  => '4',
			'orderby'  => 'date',
			'order'    => 'desc',
			'category' => '',  // Slugs
			'operator' => 'IN', // Possible values are 'IN', 'NOT IN', 'AND'.
		), $atts, 'recent_products' );
		$query_args = array(
			'post_type'           => 'product',
			'post_status'         => 'publish',
			'ignore_sticky_posts' => 1,
			'posts_per_page'      => $atts['per_page'],
			'orderby'             => $atts['orderby'],
			'order'               => $atts['order'],
			'meta_query'          => WC()->query->get_meta_query(),
			'tax_query'           => WC()->query->get_tax_query(),
		);
		$query_args = self::_maybe_add_category_args( $query_args, $atts['category'], $atts['operator'] );
		return self::product_loop( $query_args, $atts, 'recent_products' );
	}

	/**
	 * Loop over found products.
	 * @param  array $query_args
	 * @param  array $atts
	 * @param  string $loop_name
	 * @return string
	 */
private static function product_loop( $query_args, $atts, $loop_name ) {
		global $woocommerce_loop;
		$columns                     = absint( $atts['columns'] );
		$woocommerce_loop['columns'] = $columns;
		$woocommerce_loop['name']    = $loop_name;
		$transient_name              = 'wc_loop' . substr( md5( json_encode( $query_args ) . $loop_name ), 28 ) . WC_Cache_Helper::get_transient_version( 'product_query' );
		$products                    = get_transient( $transient_name );
		if ( false === $products || ! is_a( $products, 'WP_Query' ) ) {
			$products = new WP_Query( apply_filters( 'woocommerce_shortcode_products_query', $query_args, $atts, $loop_name ) );
			set_transient( $transient_name, $products, DAY_IN_SECONDS * 30 );
		}
		ob_start();
		if ( $products->have_posts() ) {
			?>

			<?php do_action( "woocommerce_shortcode_before_{$loop_name}_loop", $atts ); ?>

			<?php woocommerce_product_loop_start(); ?>

				<?php while ( $products->have_posts() ) : $products->the_post(); ?>

					<?php wc_get_template_part( 'content', 'product' ); ?>

				<?php endwhile; // end of the loop. ?>

			<?php woocommerce_product_loop_end(); ?>

			<?php do_action( "woocommerce_shortcode_after_{$loop_name}_loop", $atts ); ?>

			<?php
		} else {
			do_action( "woocommerce_shortcode_{$loop_name}_loop_no_results", $atts );
		}
		woocommerce_reset_loop();
		wp_reset_postdata();
		return '<div class="woocommerce columns-' . $columns . '">' . ob_get_clean() . '</div>';
	}



