<?php

/*
 * Plugin Name: August -content augmented plugin
 * Plugin URI:
 * Description: A test plugin to demonstrate wordpress functionality
 * Version: 0.1
 * Author: J Lee & S Lee
 * Author URI:
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Requires at least: 4.4
 * Tested up to: 4.7
 *
 * @link
 * @package August2
 * @wordpress-plugin
 * @category Core
 *
 * Text Domain: woocommerce
 * Domain Path: /i18n/languages/
 *
 */


//define

//require_once(rtrim($_SERVER['DOCUMENT_ROOT'], '/') . '/wp-load.php');  //"../../../../wp-load.php" will load all wp site with all installed plugins and themes
//include_once() will include and evaluate the specified file. If the file isn’t found, a PHP warning will be thrown.
//require_once() will throw a PHP fatal error if the file isn’t found

//보안용 초기 코드 if ( ! defined( 'WPINC' ) ) die;


class  August2 {
	
	private $version;
	
	function __construct() {
		
		$this->init();
		$this->init_hooks();
		
		do_action( 'woocommerce_loaded' );
		
		add_filter( 'the_content', array( $this, 'add_author_line_to_content' ) );
		//add_action(
		add_action( 'plugins_loaded', array( $this, 'init' )); // 이것대신에 function을 호출하는 것이 나는 더 편함
		
	}
	
	function init_hooks(){
		
		//$this->product_factory=
		//register_activation_hook( $file, $function );  //$this->activate_plugin
		register_activation_hook( __FILE__, 'august_activate' );
		//register_deactivation_hook()
		//register_uninstall_hook()// A drawback to this method is that the plugin needs to be able to run for it to work; instead, create an uninstall.php
		
		
	}  //or includes?
	
	function init(){
		
		//add_action( 'admin_menu', array( $this, 'create_plugin_settings_page' ) );  ->add_menu_page( $page_title, $menu_title, $capability, $slug, $callback, $icon, $position );
		
		if ( is_admin() ) {  //isset( $GLOBALS['current_screen'] )
			$admin = NULL; //new Object()....
			$admin->init();
		} else{
			$plugin = NULL;
			$plugin->init();
		}
		
		//
		add_shortcode('vbox',array( $this,'aug2_shortcodes'));
		
	}
  	
	function aug2_shortcodes(){
		//include_once()
		
	}
  	
}

global $august2;
$august2 = new August2();


/*
 * 이 class가 해야 할일 
 * 
 * 1)redirect - vbox or listView, productPage: add_filter (from mysql get_meta key)
 * 2)comments : productPage -add from source : add_filter or javascript? get_meta_key, gSheet
 * 3)add_shortcode : vbox
 * 
 * 4)admin page : add_action-admin_page
 * 5)import-export : button_click :add_action  : load_from_mysql, rest_api_for gSheet, gApps_script
 * 
 * 
 */

/*
 * The plugins are loaded right before theme
 * index.php or admin.php -> wp-load.php -> wp-config.php -> wp-settings.php :: WP_INC =>load plugins =>load theme(functions.php)

 * 모양을 바꿀 것들은 무엇인가?(add_action)  post/page/settings
 * 데이터를 제공해야 할것들은 무었인가?(add_filter) 혹은 변환
 * trigger events들은 무엇인가?(add_action) wp_admin_load
 *
 *
 * plugin초기화 방법 2가지
 * $class_instance = new ClassName();  or add_action( 'load-plugins.php', array( 'ClassName', 'init' ) );
 *
 *
 * wp_mail( $email, $subject, $message );
 *
 */

?>