<?php
/*
 * Plugin Name: Sample17 plugin
 * Plugin URI:
 * Description: A test plugin to demonstrate wordpress functionality
 * Version: 0.1
 * Author: J Lee Lee
 * Author URI:
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Requires at least: 4.4
 * Tested up to: 4.7
 *
 * @link
 * @package Sample17
 * @wordpress-plugin
 * @category Core
 *
 * 
 *
 */
//zip all : at parent folder: zip -r ./sample17.zip ./sample17/
//require_once(dirname(dirname(getcwd())).'wp-load.php'); for unit testing include wp-load.php
class Sample17 {	
	protected $user;
	
	function __construct() {
		//is woocommerce installed? 그러면 설치 
		register_activation_hook( __FILE__, array($this, 'plugin_activated' ) );
		register_deactivation_hook( __FILE__, array($this, 'plugin_deactivated' ) );
		
	}
	
	public function plugin_activated() {
		//echo 'I am activated - plugin Sample 17'. "\n";
		file_put_contents(__DIR__.'/my_loggg.txt','plugin_activation called'. "\n",FILE_APPEND);
		add_option( 'my_plugin_activated', time() );  //update_option, echo get_option('plugin_error');
		//is wordpress-load.php loaded?
		//is woocommerce installed & activated?
		file_put_contents(__DIR__.'/my_loggg.txt',dirname(__FILE__,2).'/woocommerce/woocommerce.php'. "\n",FILE_APPEND);
			
		if ( is_plugin_active( 'woocommerce/woocommerce.php' ) ) {
			file_put_contents(__DIR__.'/my_loggg.txt','checked true for woocommerce installed'. "\n",FILE_APPEND);
			//echo "hi I found woocommerce";//plugin is activated
		}
		flush_rewrite_rules();
		file_put_contents(__DIR__.'/my_loggg.txt','completed plugin activation'. "\n",FILE_APPEND);
	}
	public function plugin_deactivated() {
		file_put_contents(__DIR__.'/my_loggg.txt','plugin_deactivation called'. "\n",FILE_APPEND);
		//echo 'I am de-activated - plugin Sample 17'. "\n";
	}
	
	public function hello(){
		echo "hello";
		file_put_contents(__DIR__.'/my_loggg.txt','hello'. "\n",FILE_APPEND);
		$args = array(
				'public' => true,
				'label'  => 'Board Games'
		);
		register_post_type( 'boardgames', $args );
		
	}
	
	public function hello2(){
		if( is_admin() && get_option( 'my_plugin_activation' ) == 'just-activated' ) {
			delete_option( 'my_plugin_activation' );
			flush_rewrite_rules();
		}
		
	}
	
	public function modifyProductReviewTab($review_option){
		
		//is wcc_single_product && remove review tab? ->remove_action( 'woocommerce_product_tabs', 'woocommerce_product_reviews_tab', 30);
		//remove_action( 'woocommerce_product_tab_panels', 'woocommerce_product_reviews_panel', 30);
		//replacement : echo comments_template('woocommerce/single-product-reviews');
		
	}
	
	
	public function loadFunctions(){
		add_action('init', array( $this, 'hello'));
		add_action( 'admin_init',array( $this, 'hello2') );
	}
		
}

$sample17=new Sample17();
file_put_contents(__DIR__.'/my_loggg.txt','hello_plugin_called'. "\n",FILE_APPEND);

$sample17->loadFunctions();
