<?php

/*
 * 
 * https://www.smashingmagazine.com/2015/06/wordpress-custom-page-templates/
 * 
 */

if ( is_page('products') ) {
	get_header( 'shop' );
} elseif ( is_page( 42 ) ) {
	get_header( 'about' );
} else {
	get_header();
}

?>

<div id="primary" class="site-content">
    <div id="content" role="main">

      <?php while ( have_posts() ) : the_post(); ?>
        <?php get_template_part( 'content', 'page' ); ?>
        <?php comments_template( '', true ); ?>
      <?php endwhile; // end of the loop. ?>

    </div><!-- #content -->
  </div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>