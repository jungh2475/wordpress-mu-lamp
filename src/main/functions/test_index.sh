#!/usr/bin/env bash

#unit test of index.php using curl 

curl -X POST http://192.168.219.116/webapi/index2.php -d '{a:b}'


#! /usr/bin/env sh
 
### chmod +x then run test as ./sample_est.sh ###


#testing function defined in file
source ../functions.sh
 
function testWeCanWriteTests () {
	
	#load config file
	#load filescript as function then compare outputs of interest with expected values
	
	
    assertEquals "it works" "it works"
}



## Call and Run all Tests
. "../shunit2-2.1.6/src/shunit2"


####################################################



#https://code.tutsplus.com/tutorials/test-driving-shell-scripts--net-31487


#https://unwiredcouch.com/2016/04/13/bash-unit-testing-101.html
#https://blog.engineyard.com/2014/bats-test-command-line-tools
#https://github.com/sstephenson/bats
#run: $ bats addition.bats

#!/usr/bin/env bats

@test "addition using bc" {
  result="$(echo 2+2 | bc)"
  [ "$result" -eq 4 ]
}

@test "addition using dc" {
  result="$(echo 2 2+p | dc)"
  [ "$result" -eq 4 ]
}
