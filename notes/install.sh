#!/bin/bash
####################################
#
#  (0. optional virtualbox + vagrant within ubuntu
#  1.install LAMP (apache, mysql, php)  : user:k2475(????), wp-php:admin(kewtea388?), mysql:root(????),wp(????)
#  2.install wordpress-cli
#  3.install wordpress multisite
#  4.install plugins and themes
#  5.(configure the wordpress with wp-cli)
#
####################################

# vagrant+virtualbox: https://drupalize.me/videos/installing-vagrant-and-virtualbox?p=1526, https://coolestguidesontheplanet.com/getting-started-vagrant-os-osx-10-9-mavericks/
# (mac) brew cask install virtualbox -> brew cask install vagrant, brew cask install vagrant-manager
# mkdri/cd -> vagrant init hashicorp/precise64 
#	(vagrant box add hashicorp/precise64, config.vm.box = "hashicorp/precise64") -> vagrant up
#	vagrant ssh  ->......logout -> vagrant halt/suspend/destroy  ->(vagrant box remove)
#  Most Vagrant base boxes have only 2 users with SSH access, root and vagrant. They both use  vagrant as password
#  ssh -p 2222 kevin@localhost, //config.ssh.username,password,host,port, //sudo adduser jungh sudo (group), sudo deluser tester
#  config.vm.define "chef" do |chef| chef.vm.box = "chef/ubuntu-13.04"  vagrant up chef  -> vagrant halt chef
# vagrant login -> vagrant share
# vagrant box list
# to run: sudo bash install.sh or sudo ./install.sh
# (if not running, check permission: chmod +x install.sh)

# sudo apt-get update
# sudo apt-get upgrade
# install basic packages : open-ssh-server, nano, ...?
sudo apt-get install curl
# get ip address: ip addr show eth0 | grep inet | awk '{ print $2; }' | sed 's/\/.*$//'


# 1.install apache, php, mysql
# source: https://www.digitalocean.com/community/tutorials/how-to-install-linux-apache-mysql-php-lamp-stack-on-ubuntu-16-04
sudo apt-get install apache2
sudo nano /etc/apache2/apache2.conf  # edit:ServerName server_domain_or_IP ,....Inside, at the bottom of the file, add a ServerName directive, pointing to your primary domain name 
sudo apache2ctl configtest
sudo systemctl restart apache2
#now test with your browser
sudo apt-get install mysql-server
#set root password
sudo apt-get install php libapache2-mod-php php-mcrypt php-mysql
#advances on index.php over index.html
sudo nano /etc/apache2/mods-enabled/dir.conf
#<IfModule mod_dir.c>
#    DirectoryIndex index.php index.html index.cgi index.pl index.xhtml index.htm
#</IfModule>
sudo systemctl restart apache2
sudo systemctl status apache2
#php
apt-cache search php- | less
sudo apt-get install php-cli
sudo apt-get install php-all-dev, php-common, php-curl
#testing php
sudo nano /var/www/html/info.php  # with <?php phpinfo();
#php in mac
curl -s http://php-osx.liip.ch/install.sh | bash -s 7.0  #or 7.1
#config apach2 with LoadModule php5_module /usr/local/php5/libphp5.so
#.bshrc_profile, vi ~/.bash_profile : export PATH=/usr/local/php5/bin:$PATH
#sudo vi /usr/local/php5/php.d/99-liip-developer.ini
	
#install mysql 

# 2.install wordpress-cli (download->chmod->/bin wp)
curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
chmod +x wp-cli.phar
sudo mv wp-cli.phar /usr/local/bin/wp

# 2.mac wp-cli in mac osx
#brew tap josegonzalez/homebrew-php
#brew install wp-cli



# 3.install wordpress-mu (core-multisite)
# for single wordpress: 
# mkdir&cd ->wp core download -> wp core config --dbhost=<dbhost> --dbname=wordpress --dbuser=root --dbpass=root (wp-config.php)->wp core install

# wp core install --url="sites.kewtea.com"  --title="Blog Title" --admin_user="admin" --admin_password="5683882kt2475" --admin_email="admin@kewtea.com"
#source: digitlaocean: https://www.digitalocean.com/community/tutorials/how-to-install-wordpress-with-lamp-on-ubuntu-16-04
#source: digitalocean_old: https://www.digitalocean.com/community/tutorials/how-to-install-wordpress-on-ubuntu-14-04
#source: digitalocean_multisite: https://www.digitalocean.com/community/tutorials/how-to-set-up-multiple-wordpress-sites-using-multisite
#source: wp-cli: https://www.linode.com/docs/websites/cms/install-wordpress-using-wp-cli-on-ubuntu-14-04
#source: https://www.smashingmagazine.com/2015/09/wordpress-management-with-wp-cli/
#source: wp-cli multisite: wp core multisite-install
wp core multisite-install --title="Welcome to the WordPress" --admin_user="admin" --admin_password="password" --admin_email="user@example.com"
# source: wp-cli install ; https://blog.sucuri.net/2015/08/wp-cli-guide-installing-wordpress.html

wp core update
# configure mysql_db and configure.php
mysql -u root -p
#echo MYSQL> CREATE DATABASE wordpress DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
#echo MYSQL> GRANT ALL ON wordpress.* TO 'wordpressuser'@'localhost' IDENTIFIED BY 'password'; 
#echo MYSQL> FLUSH PRIVILEGES;
#echo MYSQL> EXIT;
mysql -u root -p
sudo apt-get install php-curl php-gd php-mbstring php-mcrypt php-xml php-xmlrpc
sudo systemctl restart apache2

#Enable .htaccess Overrides
sudo nano /etc/apache2/apache2.conf 
#echo <Directory /var/www/html/>
#echo    AllowOverride All
#echo </Directory>
sudo a2enmod rewrite
sudo apache2ctl configtest


# 4.install wp plugins & themes
##plugins
wp plugin install woocommerce
wp plugin activate woocommerce

##themes
wp theme install twentyten
wp theme activate twentyten


# (5. configure via wp-cli)



# configure HTTPS with apache
#source: https://www.digitalocean.com/community/tutorials/how-to-secure-apache-with-let-s-encrypt-on-ubuntu-16-04




## 6. Update & Maintenance
wp plugin update --all
wp theme update --all
