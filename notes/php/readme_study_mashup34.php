<?php
/*
* Plugin Name: K-Mashup-Plugin
* Plugin URI: https://www.sphouse.co/extra/wp-kmup
* Description: An awesome plugin you can’t do without brought to you by sphouse.
* Version: 0.1
* Text Domain: cwk-ap-textdomain
* Domain Path: /languages/
* Author: Jungh Lee
* Author URI: https://www.sphouse.co/
* License: GPLv3
* License URI: http://www.gnu.org/licenses/gpl-3.0
* Slug: kmup
*/
?>
https://codex.wordpress.org/ko:Writing_a_Plugin

please read more from https://codex.wordpress.org/Writing_a_Plugin
Saving Plugin Data to the Database: WordPress "option", Post Meta.

add_option($name, $value);  //use$value as array to save space
get_option($option);

add_post_meta( $this->id, $meta_box, $v, true );
update_post_meta($post_ID, 'poddata', $poddata);
$poddata = get_post_meta($post_ID, 'poddata');  //get_post_meta( int $post_id, string $key = '', bool $single = false )

//short code 읽기: https://codex.wordpress.org/Shortcode_API
function foobar_func( $atts ){
	return "foo and bar";
}
add_shortcode( 'foobar', 'foobar_func' );

// [bartag foo="foo-value"]  foo, bar 입력이 없으면 default값이 들어간다  , The shortcode will return as foo = {the value of the foo attribute}.
function bartag_func( $atts ) {
    $a = shortcode_atts( array(
        'foo' => 'something',
        'bar' => 'something else',
    ), $atts );

    return "foo = {$a['foo']}";
}
add_shortcode( 'bartag', 'bartag_func' );

//add_filter: https://codex.wordpress.org/Plugin_API/Filter_Reference, http://adambrown.info/p/wp_hooks/hook


//php log & debugging
tail -f /tmp/php-errors
error_log ('your_content', 3, 'your_log_file');
php -a //php shell
via browser:echo '<script type="text/javascript">' echo 'console.log = console.log || function(){};';
https://www.codeforest.net/debugging-php-in-browsers-javascript-console
function debug($name, $var = null, $type = LOG) {
    echo '<script type="text/javascript">'.NL;
    switch($type) {
        case LOG:
            echo 'console.log("'.$name.'");'.NL;    
        break;
        case INFO:
            echo 'console.info("'.$name.'");'.NL;    
        break;
        case WARN:
            echo 'console.warn("'.$name.'");'.NL;    
        break;
        case ERROR:
            echo 'console.error("'.$name.'");'.NL;    
        break;
    }
    echo '</script>'.NL;


require_once("PHPDebug.php");
$debug = new PHPDebug();
$debug->debug("A very simple message");
<?php
	echo "<h1>Hello, PHP-7!</h1>";
	echo "<br";
	
	phpinfo();
	print("<br/>");
	
	$username = $_GET['username'] ?? 'not passed';
	print($username);
	print("<br/>");
	
	// Equivalent code using ternary operator
	$username = isset($_GET['username']) ? $_GET['username'] : 'not passed';
	print($username);
	print("<br/>");
	// Chaining ?? operation
	$username = $_GET['username'] ?? $_POST['username'] ?? 'not passed';
	print($username);
	
?>


echo function_exists( 'register_nav_menus' );
wp_list_pages();
echo function_exists('wp_list_pages');
<?php wp_list_pages('include=4,5'); ?>
bloginfo('url'); http://example.com
bloginfo('url');http://192.168.0.10
bloginfo('admin_email');
bloginfo('template_url'); http://192.168.0.10/wp-content/themes/twentyseventeen


isset($_GET['username'])

have_posts()

query_posts('category_name=Featured&posts_per_page=3');
the_title();
the_permalink(); 
the_content();
the_excerpt();
the_category();
the_tags();
the_time();


get_post_meta($post->ID, ‘keyName’, true);
which is a function for getting custom fields
 