#!/bin/bash
#!/usr/bin/env bash


echo "[kew-log] hi i am installing LAMP, wp-cli "

#만약 한국에서 설치한다면  
sudo rm -r /var/lib/apt/lists/*
sudo apt-get clean	
sudo sed -i 's/archive.ubuntu.com/ftp.daum.net/g' /etc/apt/sources.list

#chown 660
#include at Vagrantfile: Vagrant.configure("2") do |config| ....config.vm.provision :shell, path: "bootstrap.sh"
#config.vm.network :forwarded_port, guest: 80, host: 4567, config.vm.network "public_network"

sudo apt-get update
#apt-get install -y apache2
sudo apt-get install -y apache2 apache2-utils 
sudo systemctl enable apache2
sudo systemctl start apache2


#
#if ! [ -L /var/www ]; then
#  rm -rf /var/www
#  ln -fs /vagrant /var/www
#fi

lsb_release -a
whoami #ubuntu ubuntu
hostname -I
#ifconfig eth1 192.168.0.17 netmask 255.255.255.0 up

#sudo service apache2 start

########### install php, python, mysql...
sudo apt-get install -y python3
sudo apt-get install -y python3-pip
sudo apt-get install -y build-essential libssl-dev libffi-dev python3-dev
sudo apt-get install -y python3-venv
python3 -V

sudo apt-get update

sudo apt-get install php7.0 php7.0-mysql libapache2-mod-php7.0 php7.0-cli php7.0-cgi php7.0-gd  
sudo apt-get install -y php-curl php-gd php-mbstring php-mcrypt php-xml php-xmlrpc
sudo apt install -y php7.0-cli
sudo systemctl restart php7.0-fpm
#apt-get install php7-nightly
#sudo vi /var/www/html/info.php (http://server_address/info.php) -><?php phpinfo();?>

echo "[kew-log] wp-cli core will install mysql if not installed yet"


########### install wp-cli

cd /tmp

#curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
wget https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
chmod +x wp-cli.phar
sudo mv wp-cli.phar /usr/local/bin/wp
wp --info
wp help


#wp core download

########### setup apache for wordpress


#예전에는  이렇게 cd /tmp...curl -O https://wordpress.org/latest.tar.gz...tar -xzvf latest.tar.gz 
#...sudo cp -a /tmp/wordpress/. /var/www/html...sudo chown -R www-data:www-data /var/www/html...sudo chmod -R 755 /var/www/html/  ...sudo chmod g+w /var/www/html/wp-content
#sudo rsync -av wordpress/* /var/www/html/

########### mysql setup

#install mysql 5.7 on ubuntu 14.04
#wget http://dev.mysql.com/get/mysql-apt-config_0.6.0-1_all.deb
#sudo dpkg -i mysql-apt-config_0.6.0-1_all.deb
sudo apt-get update

sudo apt-get install -y mysql-server
sudo mysql_secure_installation
#sudo mysql_install_db

mysql --version
service mysql status
echo "######################"
echo "CREATE DATABASE `wp1` CHARACTER SET utf8 COLLATE utf8_general_ci;"
echo "GRANT ALL PRIVILEGES ON wp1.* TO 'wpadmin'@'localhost' IDENTIFIED BY '1234';"
echo "FLUSH PRIVILEGES;EXIT;"
echo "######################"
mysql -u wpadmin -p

#CREATE DATABASE `wp1` CHARACTER SET utf8 COLLATE utf8_general_ci;
#GRANT ALL PRIVILEGES ON wp1.* TO 'wpadmin'@'localhost' IDENTIFIED BY '1234';
#grant all on wordpress.* to 'wpadmin' identified by 'password';
#FLUSH PRIVILEGES;EXIT;

sudo systemctl restart apache2.service 
sudo systemctl restart mysql.service

######### apache www

#cd /var/www/html/example.com
#sudo chown -R www-data public_html
#sudo usermod -aG www-data username
#sudo chmod -R g+w public_html


echo "#####################"
echo "#### install wp via wp-cli ####"

sudo -u www-data wp core download
sudo -u www-data wp core config --dbname=wp1 --dbuser=wpadmin --dbpass=1234 --dbhost=localhost --dbprefix=wp_
sudo -u www-data wp core install --url="http://example.com" --title="Blog Title" --admin_user="wpadmin" --admin_password="1234" --admin_email="jungh.lee@kewtea.com"


wp plugin install wordpress-seo
wp plugin activate wordpress-seo
wp plugin list
wp plugin update --all

wp theme list

wp core update
wp core update-db

echo "##################################"
echo "##### end of wp installation  ####"
echo "##################################"
