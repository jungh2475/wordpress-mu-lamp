





/*

 view
*/

function onOpen() {
  var ui = SpreadsheetApp.getUi();
  // Or DocumentApp or FormApp.
  ui.createMenu('Custom Testing Menu')
      .addItem('First item', 'requestUpdate')
      .addSeparator()
      .addSubMenu(ui.createMenu('Sub-menu')
          .addItem('Second item', 'showSidebar'))
      .addToUi();
}


function showSidebar() {
  var html = HtmlService.createHtmlOutputFromFile('Page')
      .setTitle('My custom sidebar')
      .setWidth(300);
  SpreadsheetApp.getUi() // Or DocumentApp or FormApp.
      .showSidebar(html);
}

/*

custom functions (=functionName within cell)

*/

function requestUpdate(){
  var ui = SpreadsheetApp.getUi();
  ui.alert('Confirmation received.');
  //console.log("Iam called");
}

function importSettings(){
  var settingValues="34";
  return settingValues;
}

function doubleSum(input){
  return input*2;
}

var Watcher = function(name){
  
  this.version = name;
  this.hello1 = function(){
    return "hihihi4..";
  }
  
  this.hello2 = function(){
    Logger.log("hi..");
    return "hello";
  }

}

var watch2= new Watcher("jungh");

function test(){  
  var result = watch2.hello1();
  return result;
}

function test2(){
  var result="hi";
  
  var data = UrlFetchApp.fetch(URL, googleOAuth_()).getContentText();
  var xmlOutput = Xml.parse(data, false);
  var albums = xmlOutput.getElement().getElements('entry');
  
  result=result+"";
  return result;

}


function getJsonFromAPI(url,method,payload) {
  var method=method;  //'post', default is 'get'
  var payload=payload;  //{'field':'value',.....
  
  var param = {
   'method' : method, 
   'contentType': 'application/json',  
   'payload' : JSON.stringify(payload)
 };  //headers
  var response  = UrlFetchApp.fetch(url,param);
  Logger.log(response.getContentText());
  var json = JSON.parse(response);
  return json;
}

/*

open 할때  -> menu, sidebar 추가 

post-query(import-준비) -> 서버쪽에서 자료 업데이트 하도록 준비
get(import) -> 현재 마지막으로 저장된 결과를 가져 오기
get(import)- status 현재 세팅 값들을 읽어 온다 




*/