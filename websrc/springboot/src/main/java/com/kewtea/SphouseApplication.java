package com.kewtea;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SphouseApplication {

	public static void main(String[] args) {
		SpringApplication.run(SphouseApplication.class, args);
	}
}
